import 'package:flutter/material.dart';
import 'package:tese1/answer.dart';
import 'package:tese1/question.dart';
import 'package:tese1/result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;

  final _questions = [
    {
      "question": 'Who invented gravity?',
      "answers": ["Albert Einstein", "Issac Newton", "Dr. Homi Bhaba"],
      "correct_answer": "Issac Newton"
    },
    {
      "question": 'Where is Status of Unity located?',
      "answers": ["USA", "India", "Australia"],
      "correct_answer": "India"
    },
    {
      "question": 'Which institute was started by Dr. Vikram Sarabhai?',
      "answers": [
        "Tata Institute of Fundamental Research",
        "Physical Research Laboratory",
        "Indian Institute of Science, Bangalore"
      ],
      "correct_answer": "Physical Research Laboratory"
    },
  ];

  var final_results = [];

  void _answerQuestion(answer, context) {
    var current_question = this._questions.elementAt(_questionIndex);
    var result = {
      "question": current_question['question'],
      "answered": answer,
      "correct_answer": current_question['correct_answer'],
      "answered_correct": current_question['correct_answer'] == answer
    };

    setState(() {
      if (_questions.length > _questionIndex + 1){
        _questionIndex += 1;
      }
      final_results.add(result);
    });
    if (final_results.length == _questions.length){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => Result(final_results)));
    }
  }

  List<Widget> renderStage(context) {
    /// This method renders the question and answers into the display.
    List<Widget> widgets = [];
    var currentQuestion = _questions.elementAt(_questionIndex);
    widgets.add(
        Question(_questions[_questionIndex]['question']?.toString() ?? ''));
    var answers = currentQuestion['answers'] as List<String>;
    answers.forEach((answerText) =>
        {widgets.add(Answer(answerText, _answerQuestion, context))});
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(this.renderStage),
    );
  }
}

class HomePage extends StatelessWidget {
  var renderStage;

  HomePage(this.renderStage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter App"),
      ),
      body: Column(
        children: renderStage(context),
      ),
    );
  }
}
