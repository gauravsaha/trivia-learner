import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  var answerText;
  var onPressCallable;
  var context;

  Answer(this.answerText, this.onPressCallable, this.context);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () => {this.onPressCallable(this.answerText, this.context)},
        child: Text(this.answerText));
  }
}
