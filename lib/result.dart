import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  var results;

  Result(this.results);

  List<Widget> renderResults() {
    List<Widget> resultItems = [];
    // results = results as List<String>;
    results.forEach((element) => {
          resultItems.add(AnswerItem(
              element["question"],
              element['correct_answer'],
              element['answered'],
              element['answered_correct']))
        });
    return resultItems;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Result"),
      ),
      body: Column(
        children: renderResults(),
      ),
    );
  }
}

class AnswerItem extends StatelessWidget {
  String questionText;
  String submittedAnswer;
  String correctAnswer;
  bool answeredCorrect = false;

  AnswerItem(this.questionText, this.correctAnswer, this.submittedAnswer, this.answeredCorrect);

  TextStyle renderAnsweredQuestion(){
    return TextStyle(
      decoration: !this.answeredCorrect ? TextDecoration.lineThrough : TextDecoration.none,
      color: !this.answeredCorrect ? Colors.red : Colors.green
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            this.questionText,
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
          Text(this.correctAnswer,
              style: TextStyle(
                color: Colors.green,
              )),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Text(this.submittedAnswer, style: renderAnsweredQuestion()),
          )
        ],
      ),
    );
  }
}

